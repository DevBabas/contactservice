#!/bin/bash

# Script to check SonarCloud Quality Gate result

echo "Waiting for SonarCloud Quality Gate result..."
sleep 5s  # Wait for a short period to ensure the SonarCloud processing is done

# Ensure that SONAR_TOKEN and SONAR_PROJECT_KEY are set as environment variables
if [ -z "$SONAR_TOKEN" ] || [ -z "$SONAR_HOST_URL" ]; then
    echo "Error: Environment variables SONAR_TOKEN and SONAR_PROJECT_KEY must be set."
    exit 1
fi

# Fetching the Quality Gate status from SonarCloud
status=$(curl -s -u "$SONAR_TOKEN": "https://sonarcloud.io/api/qualitygates/project_status?projectKey=$SONAR_HOST_URL" | jq -r '.projectStatus.status')
echo "Quality Gate status: $status"

# Check if the Quality Gate status is OK
if [ "$status" != "OK" ]; then
    echo "Quality Gate failed"
    exit 1
else
    echo "Quality Gate passed"
fi
